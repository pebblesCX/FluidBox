all:
	gcc -g -o build/FluidBox FluidBox.c `pkg-config --cflags --libs cairo` `pkg-config --cflags --libs gtk+-3.0` -lm
run: all
	./build/FluidBox
