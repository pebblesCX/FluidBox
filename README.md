# Fluid 2D

A lightweight classical 2D fluid simulator in C. Supports Lennard-Jones and Coulomb force-fields with full multi-body interactions.

![Example animation](https://gitea.com/pebblesCX/FluidBox/raw/branch/master/docs/example.gif)

## Usage

Left click to add a particle. Right click to clear the system. Initial state can be tweaked in `initial_state` function.

## Building

Requires a C compiler with GTK and Cairo libraries available as the only dependencies. The Makefile will use `pkg-config` to find the appropriate library flags. Linux is recommended, but is also tested to work on Windows with MINGW or WSL with X forwarding.

To compile:
```
make
```

To compile and run:
```
make run
```
