#include <cairo.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <math.h>

// Physics and graphing intervals (ms)
#define PHYS_INTERVAL 10
#define GRAPH_INTERVAL 10

// Simulation/canvas dimensions
#define X_MAX 600
#define Y_MAX 450

// Maximum particle count
#define N 100

// Timestep
#define DT 1

// Sensible parameters to prevent explosions
#define MIN_DIST 20
#define V_MAX 50

// Default charge and mass
#define DEFAULT_MASS 1
#define DEFAULT_CHARGE 1

// Coulomb parameter
#define K 100

// LJ parameters
#define LJ_SIGMA 20.0
#define LJ_EPSILON 1.0

static void do_drawing(cairo_t *);
void clear_all(void);

// System state
struct
{
	int count;
	double posx[N];
	double posy[N];
	double velx[N];
	double vely[N];
	double m[N];
	double c[N];
	double Fx[N][N];
	double Fy[N][N];
} entities;

static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data)
{
    do_drawing(cr);

    return FALSE;
}

// Draw all the particles
static void do_drawing(cairo_t *cr)
{
    cairo_set_source_rgb(cr, 0, 1, 0);
    cairo_set_line_width(cr, 15);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);

    int i, j;
    for (i = 0; i <= entities.count - 1; i++ ) {
        cairo_move_to(cr, entities.posx[i], entities.posy[i]);
        cairo_line_to(cr, entities.posx[i], entities.posy[i]);
    }

    cairo_stroke(cr);    
}

// Distance accounting for periodic boundaries
double dist(double max, double q1, double q2)
{
    if (q2 - q1 > max/2)
        return -max + q2 - q1;
    else if (q2 - q1 < -max/2)
        return max + q2 - q1;
    else
        return q2 - q1;
}

// Pythagorean metric
double pythag(double x, double y)
{
	return sqrt(pow(x, 2) + pow(y, 2));
}

// Check if point is within MIN_DIST of any particle
gboolean check_distances(double x, double y)
{
	for (int i = 0; i < entities.count; i++)
	{
		 if (MIN_DIST > pythag(dist(X_MAX, entities.posx[i], x), 
				 dist(Y_MAX, entities.posy[i], y)))
			 return (FALSE);
	}
	return (TRUE);
}

// Handle click event
static gboolean clicked(GtkWidget *widget, GdkEventButton *event,
    gpointer user_data)
{
    // Add particle on left click
    if (event->button == 1)
    {
	if (check_distances(event->x, event->y) && entities.count < N)
	{
		entities.posx[entities.count] = event->x;
		entities.posy[entities.count++] = event->y;
	}
    }

    // Clear everything on right click
    if (event->button == 3)
    {
        entities.count = 0;
        clear_all();
    }
    gtk_widget_queue_draw(widget);

    return TRUE;
}

double ring(double min, double max, double val)
{
    if (val < min)
    {
        int mult = 1 + floor((min - val) / (max - min));
        val += mult*(max - min);
    }
    return fmod(val, (float)max);
}

// Calculate Lennard-Jones (6-12) potential matrix
void lj_matrix(void)
{
    for (int i = 0; i < entities.count; i++)
    {
        for (int j = entities.count-1; j >= i; j--)
        {
            if (i != j)
            {
                double dx = dist(X_MAX, entities.posx[i], entities.posx[j]);
                double dy = dist(Y_MAX, entities.posy[i], entities.posy[j]);
                double d = sqrt(pow(dx,2)+pow(dy,2));
                if (d < MIN_DIST) d = MIN_DIST;
		double sigma_by_d = LJ_SIGMA/d;
                double F = -48*LJ_EPSILON*(pow(sigma_by_d, 13) - pow(sigma_by_d, 7))/LJ_SIGMA;

                entities.Fx[i][j] = -1*F*dx/d;
                entities.Fx[j][i] = F*dx/d;
                entities.Fy[i][j] = -1*F*dy/d;
                entities.Fy[j][i] = F*dy/d;
            }
        }
    }
}

// Calculate Coulomb potential matrix
void coulomb_matrix(void)
{
    for (int i = 0; i < entities.count; i++)
    {
        for (int j = entities.count-1; j >= i; j--)
        {
            if (i != j)
            {
                double dx = dist(X_MAX, entities.posx[i], entities.posx[j]);
                double dy = dist(Y_MAX, entities.posy[i], entities.posy[j]);
                double d = sqrt(pow(dx,2)+pow(dy,2));
                if (d < MIN_DIST) d = MIN_DIST;
                double F = K*entities.c[i]*entities.c[j]/pow(d, 2);

                entities.Fx[i][j] = -1*F*dx/d;
                entities.Fx[j][i] = F*dx/d;
                entities.Fy[i][j] = -1*F*dy/d;
                entities.Fy[j][i] = F*dy/d;
            }
        }
    }
}

gint physics_timestep (GtkWidget* widget)
{
    lj_matrix(); // Change your potential here
    // Update velocities (No fancy RK methods)
    for (int i = 0; i < entities.count; i++)
    {
        for (int j = 0; j < entities.count; j++)
        {
            entities.velx[i] += DT*entities.Fx[i][j]/entities.m[i];
            entities.vely[i] += DT*entities.Fy[i][j]/entities.m[i];
        }
        if (entities.velx[i] > V_MAX)
		entities.velx[i] = copysign(entities.velx[i], V_MAX);
        if (entities.vely[i] > V_MAX)
		entities.vely[i] = copysign(entities.vely[i], V_MAX);
        entities.posx[i] += entities.velx[i];
        entities.posy[i] += entities.vely[i];
        entities.posx[i] = ring(0, X_MAX, entities.posx[i]);
        entities.posy[i] = ring(0, Y_MAX, entities.posy[i]);
    }
    return (TRUE);
}

gint draw_tick (GtkWidget* widget)
{
    gtk_widget_queue_draw(widget);
    return (TRUE);
}

// Clear the canvas
void clear_all(void)
{
    for (int i = 0; i < N; i++)
    {
        entities.posx[i] = 0.0;
        entities.posy[i] = 0.0;
        entities.velx[i] = 0.0;
        entities.vely[i] = 0.0;
        entities.m[i] = 1.0;
        entities.c[i] = 1.0;
        for (int j = 0; j < N; j++)
        {
            entities.Fx[i][j] = 0.0;
            entities.Fy[j][i] = 0.0;
        }
    }
}

// Initialize system state
void initial_state(void)
{
    clear_all();

    entities.posx[0] = X_MAX/2;
    entities.posy[0] = Y_MAX/2;
    entities.velx[0] = 3;
    entities.vely[0] = 3;
    entities.m[0] = DEFAULT_MASS;
    entities.c[0] = DEFAULT_CHARGE;
    entities.count = 1;
}

int main(int argc, char *argv[])
{
    initial_state();
    GtkWidget *window;
    GtkWidget *darea;


    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    darea = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(window), darea);

    gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);
    GdkRGBA color = {0.1, 0.1, 0.1, 1};
    gtk_widget_override_background_color(window, GTK_STATE_FLAG_NORMAL, &color);

    g_signal_connect(G_OBJECT(darea), "draw", 
    G_CALLBACK(on_draw_event), NULL); 
    g_signal_connect(window, "destroy",
    G_CALLBACK(gtk_main_quit), NULL);  

    g_signal_connect(window, "button-press-event", 
    G_CALLBACK(clicked), NULL);

    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), X_MAX, Y_MAX); 
    gtk_window_set_title(GTK_WINDOW(window), "FluidBox");

    g_timeout_add(PHYS_INTERVAL, (gpointer)physics_timestep, window);
    g_timeout_add(GRAPH_INTERVAL, (gpointer)draw_tick, window);
    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
